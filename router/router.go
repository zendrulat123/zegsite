package routes

import (
	"github.com/labstack/echo/v4"
	userCreation "gitlab.com/zendrulat123/zegsite/db/createuser"
	form "gitlab.com/zendrulat123/zegsite/handler/form"
	house "gitlab.com/zendrulat123/zegsite/handler/games/house"
	outside "gitlab.com/zendrulat123/zegsite/handler/games/outside"

	//	home "gitlab.com/zendrulat123/zegsite/handler/home"
	message "gitlab.com/zendrulat123/zegsite/handler/message"
	gamesocket "gitlab.com/zendrulat123/zegsite/websocket/messagesocket"

	intro "gitlab.com/zendrulat123/zegsite/handler/intro"
)

//Routes is for routing
func Routes(e *echo.Echo) {
	//e.GET("/", home.Home)     //home
	e.GET("/form", form.Form) //form

	e.GET("/intro", intro.Intro)                    //intro
	e.GET("/house", house.House)                    //house
	e.GET("/outside/:color/:name", outside.Outside) //home
	e.GET("/ws*", gamesocket.Gamesocket)            //home
	e.GET("/ss", message.Message)                   //home

	//post
	e.POST("/processform", userCreation.UserCreation) //processing
}
