module gitlab.com/zendrulat123/zegsite

go 1.16

require (
	github.com/go-playground/validator/v10 v10.9.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gorilla/websocket v1.4.2
	github.com/labstack/echo/v4 v4.6.1
	github.com/labstack/gommon v0.3.0
	golang.org/x/net v0.0.0-20211015210444-4f30a5c0130f // indirect
	golang.org/x/sys v0.0.0-20211019181941-9d821ace8654 // indirect
)
