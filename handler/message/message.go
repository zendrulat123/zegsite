package basics

import (
	"net/http"

	"github.com/labstack/echo/v4"
	sharedclients "gitlab.com/zendrulat123/zegsite/websocket/socketparts"
)

func Message(c echo.Context) error {

	// clientid := c.Param("client")
	// color := c.Param("color")
	// name := c.Param("name")
	//gopher := clientguy.Gopher{Name: name, Color: color}

	//
	return c.Render(http.StatusOK, "message.html", map[string]interface{}{
		"Clients": sharedclients.ClientShared,
		//"guys":    gopher,
	})

}
