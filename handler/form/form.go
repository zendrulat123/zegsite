package basics

import (
	"net/http"

	"github.com/labstack/echo/v4"
	alluser "gitlab.com/zendrulat123/zegsite/db/alluser"
)

func Form(c echo.Context) error {

	au := alluser.GetAllDataUser()
	return c.Render(http.StatusOK, "form.html", map[string]interface{}{
		"all": au,
	})

}
