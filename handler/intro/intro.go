package intro

import (
	"net/http"

	"github.com/labstack/echo/v4"
	
)

func Intro(c echo.Context) error {
	return c.Render(http.StatusOK, "introduction.html", map[string]interface{}{
		
	})

}
