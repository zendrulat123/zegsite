package outside

import (
	"net/http"

	"github.com/labstack/echo/v4"
	clientguy "gitlab.com/zendrulat123/zegsite/handler/games/clientguy"
)

func Outside(c echo.Context) error {
	color := c.Param("color")
	name := c.Param("name")
	gopher := clientguy.Gopher{Name: name, Color: color}
	return c.Render(http.StatusOK, "outside.html", map[string]interface{}{
		"guys": gopher,
	})
}
