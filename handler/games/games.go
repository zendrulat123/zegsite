package games

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func Games(c echo.Context) error {
	return c.Render(http.StatusOK, "games.html", map[string]interface{}{})

}
