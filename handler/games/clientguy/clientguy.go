package clientguy

import "github.com/gorilla/websocket"

type Gopher struct{
	Name string
	Color string
}

type Clientguy struct {
	ID     string
	Conn   *websocket.Conn
	Pool   *Pool
	Color  string
	Points int
}
type Pool struct {
	Register   chan *Clientguy
	Unregister chan *Clientguy
	Clients    map[*Clientguy]bool
	Broadcast  chan Message
}

type Message struct {
	Type     int    `json:"type"`
	Body     string `json:"body"`
	ClientID string `json:"clientid"`
}
