package autho

import (
	"database/sql/driver"
	"fmt"
	"reflect"

	"github.com/go-playground/validator/v10"
)

// use a single instance of Validate, it caches struct info
var validate *validator.Validate

func ValidateStruct(u interface{}) {

	validate = validator.New()

	// returns InvalidValidationError for bad validation input, nil or ValidationErrors ( []FieldError )
	err := validate.Struct(u)
	if err != nil {
		if _, ok := err.(*validator.InvalidValidationError); ok {
			fmt.Println(err)
			return
		}

		fmt.Println("------ List of tag fields with error ---------")

		for _, err := range err.(validator.ValidationErrors) {
			fmt.Println(err.StructField())
			fmt.Println(err.ActualTag())
			fmt.Println(err.Kind())
			fmt.Println(err.Value())
			fmt.Println(err.Param())
			fmt.Println("---------------")
		}

	}

	// save user to database
}

// ValidateValuer implements validator.CustomTypeFunc
func ValidateValuer(field reflect.Value) interface{} {

	if valuer, ok := field.Interface().(driver.Valuer); ok {

		val, err := valuer.Value()
		if err == nil {
			fmt.Println(err)
			return val
		}
		// handle the error how you want
	}

	return nil
}

type User struct {
	ID    string `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Name  string `param:"name" query:"name" form:"name" json:"name" xml:"name"`
	Email string `param:"email" query:"email" form:"email" json:"email" xml:"email" validate:"required,email" mod:"trim"`
}
