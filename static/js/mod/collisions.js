//collusion with a popup
export function Coll(o, o2, p) {
    var bf = o.offset();
    var bf2 = o2.offset();
    var bfleft = bf.left;
    var bfright = bf.left + o.outerWidth();
    var bfbottom = bf.top + o.outerHeight();
    var bftop = bf.top;
    var bf2left = bf2.left;
    var bf2right = bf2.left + o2.outerWidth();
    var bf2bottom = bf2.top + o2.outerHeight();
    var bf2top = bf2.top;
    console.log(bfleft, "<", bf2left, "&&", bfright, "<", bf2right, "&&", bftop, "<", bf2top, "&&", bfbottom, ">", bf2bottom)

    if (bfleft < bf2left && bfright < bf2right && bftop < bf2top && bfbottom > bf2bottom) {
        //crash
        $(p).addClass("active");
    } //end of collision

}
//collusion with wall
export function Hitwall(o, o2) {
    var bf = o.offset();
    var bf2 = o2.offset();
    var bfleft = bf.left;
    var bfright = bf.left + o.outerWidth();
    var bfbottom = bf.top + o.outerHeight();
    var bftop = bf.top;
    var bf2left = bf2.left;
    var bf2right = bf2.left + o2.outerWidth();
    var bf2bottom = bf2.top + o2.outerHeight();
    var bf2top = bf2.top;
    // console.log(bfleft, ">", bf2left, "&&", bfright, "<", bf2right, "&&", bftop, "<", bf2top, "&&", bfbottom, ">", bf2bottom)
    if (bfleft < bf2left && bfright < bf2right && bftop < bf2top && bfbottom > bf2bottom) {
        //crash
        var pushs = o.position().top - o2.position().top + o.position().left - o2.position().left;
        o.css('top', pushs);
    } //end of collision
}

//collusion two obj
export function Collect(o, o2) {
    var bf = o.offset();
    var bf2 = o2.offset();
    var bfleft = bf.left;
    var bfright = bf.left + o.outerWidth();
    var bfbottom = bf.top + o.outerHeight();
    var bftop = bf.top;
    var bf2left = bf2.left;
    var bf2right = bf2.left + o2.outerWidth();
    var bf2bottom = bf2.top + o2.outerHeight();
    var bf2top = bf2.top;
    if (bfleft < bf2left && bfright < bf2right || bftop < bf2top && bfbottom > bf2bottom) {
        //crash
        o.append(o2);
        o2.css("top", 0);
        o2.css("left", 0);
    } //end of collision
}