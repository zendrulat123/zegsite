import { Coll, Hitwall } from './mod/collisions.js';
import { Release } from './mod/actions.js';

$(document).ready(function() {
    //store stuff
    var pane = $('#pane'),
        box = $('.box'),
        boximg = $('.box img'),
        box2 = $('#box2'),
        box3 = $('#box3'),
        box4 = $('#box4'),

        wall = $('.wall'),
        wh = pane.width() - box.width(),
        wv = pane.height() - box.height(),
        d = {},
        x = 5;


    /****************************
    auxiliary setup for actions
    ******************************/
    //closing popup
    $(".close").on("click", function() {
        box.css("top", "0%");
        box.css("left", "0%");
        $(".popup-overlay, .popup-content").removeClass("active");
        $(".popup-overlayrock, .popup-contentrock").removeClass("active");
    });
    //ensure limits on the offsets
    function newh(v, a, b) {
        var n = parseInt(v, 10) - (d[a] ? x : 0) + (d[b] ? x : 0);
        return n < 0 ? 0 : n > wh ? wh : n;
    }

    function newv(v, a, b) {
        var n = parseInt(v, 10) - (d[a] ? x : 0) + (d[b] ? x : 0);
        return n < 0 ? 0 : n > wv ? wv : n;
    }

    //repaint
    setInterval(function() {
        box.css({
            left: function(i, v) {
                return newh(v, 37, 39);
            },
            top: function(i, v) {
                return newv(v, 38, 40);
            }
        });
        Hitwall(box, wall);
        //sizes
        wh = pane.width() - box.width();
        wv = pane.height() - box.height();

    }, 90); //end of repaint


    //keys
    $(window).keydown(function(e) {
        switch (e.which) {
            case 39: // right
                boximg.attr('src', "/static/img/walk.gif");
                break;
            case 38: // up
                boximg.attr('src', "/static/img/up.gif");
                break;
            case 37: // left
                boximg.attr('src', "/static/img/back.gif");
                break;
            case 40: // down
                boximg.attr('src', "/static/img/down.gif");
                break;
            case 32: // spacebar
                var o = box;
                var bf = o.offset();
                var bfleft = bf.left;
                var bfright = bf.left + o.outerWidth();
                var bfbottom = bf.top + o.outerHeight();
                var bftop = bf.top;
                console.log()
                $(".d").each(function(index) {
                    console.log(index + ": " + $(this).attr("class") + ": " + $(this));
                    var o2 = $(this);
                    var bf2 = o2.offset();
                    var bf2left = bf2.left;
                    var bf2right = bf2.left + o2.outerWidth();
                    var bf2bottom = bf2.top + o2.outerHeight();
                    var bf2top = bf2.top;
                    // console.log(bfleft, ">", bf2left, "&&", bfright, "<", bf2right, "&&", bftop, ">", bf2top, "&&", bfbottom, "<", bf2bottom)

                    // if (bfleft >= bf2left) {
                    //     console.log("left worked");
                    //     if (bfright <= bf2right) {
                    //         console.log("right worked");
                    //         if (bftop >= bf2top) {
                    //             console.log("top worked");
                    //             if (bfbottom <= bf2bottom) {
                    //                 console.log("bottom worked");
                    //                 console.log("congrats", $(this).attr("class"));
                    //             } else { console.log("btm didnt work", bfbottom, " ", bf2bottom) }
                    //         } else { console.log("top didnt work", bftop, " ", bf2top) }
                    //     } else { console.log("right didnt work", bfright, " ", bf2right) }
                    // } else { console.log("left didnt work", bfleft, " ", bf2left) }

                    if (bfleft >= bf2left && bfright <= bf2right && bftop >= bf2top && bfbottom <= bf2bottom) {
                        console.log("worked crassh");

                        if (o2.is(box2)) {
                            $(".popup-overlay").addClass("active");
                            $(".popup-content").addClass("active");

                        } else if (o2.is(box3)) {
                            $(".popup-overlayrock").addClass("active");
                            $(".popup-contentrock").addClass("active");
                        } else if (o2.is(box4)) {
                            $(".popup-overlayserver").addClass("active");
                            $(".popup-contentserver").addClass("active");
                        } else {}
                    }
                    // Colls(box, bb);



                });

                // Release(box4);
                // Coll(box, box2, ".popup-overlay, .popup-content");
                // Coll(box, box3, ".popup-overlayrock, .popup-contentrock");
                // Coll(box, box4, ".popup-overlayserver, .popup-contentserver");
                break;
            case 13: // entor
                var o = box;
                var bf = o.offset();
                var bfleft = bf.left;
                var bfright = bf.left + o.outerWidth();
                var bfbottom = bf.top + o.outerHeight();
                var bftop = bf.top;



                var bfs = box3.offset();
                var bflefts = bfs.left;
                var bfrights = bfs.left + box3.outerWidth();
                var bfbottoms = bfs.top + box3.outerHeight();
                var bftops = bfs.top;

                console.log("guy left more:" + bfleft, " rightless:", bfright + " botmless:" + bfbottom, " topmore:" + bftop);
                console.log("home left more:" + bflefts, " rightless:", bfrights + " botmless:" + bfbottoms, " topmore:" + bftops);
                // Release(box4);
                Coll(box, box2, ".popup-overlay, .popup-content");
                Coll(box, box3, ".popup-overlayrock, .popup-contentrock");
                Coll(box, box4, ".popup-overlayserver, .popup-contentserver");
                break;
            default:
                boximg.attr('src', "/static/img/glogo.gif");
        }
        d[e.which] = true;
    });
    $(window).keyup(function(e) {
        d[e.which] = false;
    });

    function Colls(o, o2) {
        console.log(o2);

        var bf = o.offset();
        var bf2 = o2.offset();
        var bfleft = bf.left;
        var bfright = bf.left + o.outerWidth();
        var bfbottom = bf.top + o.outerHeight();
        var bftop = bf.top;
        var bf2left = bf2.left;
        var bf2right = bf2.left + o2.outerWidth();
        var bf2bottom = bf2.top + o2.outerHeight();
        var bf2top = bf2.top;
        console.log(bfleft, "<", bf2left, "&&", bfright, "<", bf2right, "&&", bftop, ">", bf2top, "&&", bfbottom, "<", bf2bottom)

        if (bfleft < bf2left && bfright < bf2right && bftop > bf2top && bfbottom < bf2bottom) {
            //crash

            if (o2.is(box2)) {
                $(".popup-overlay").addClass("active");
                $(".popup-content").addClass("active");

            } else if (o2.is(box3)) {
                $(".popup-overlayrock").addClass("active");
                $(".popup-contentrock").addClass("active");
            } else if (o2.is(box4)) {
                $(".popup-overlayserver").addClass("active");
                $(".popup-contentserver").addClass("active");
            } else {}

        } //end of collision
    }
});