package messagesocket

import (
	"fmt"
	"net/http"

	"github.com/gorilla/websocket"
	"github.com/labstack/echo/v4"
	websockets "gitlab.com/zendrulat123/zegsite/websocket/socketparts"
)

//https://betterprogramming.pub/implementing-websocket-with-go-and-react-b3ee976770ab
func Gamesocket(c echo.Context) error {

	pool := websockets.NewPool()
	go pool.Start()
	fmt.Println("WebSocket Endpoint Hit")
	conn, err := upgrader.Upgrade(c.Response(), c.Request(), nil)
	if err != nil {
		return err
	}
	defer conn.Close()

	client := &websockets.Client{
		Conn: conn,
		Pool: pool,
	}

	pool.Register <- client
	client.Read()

	return nil
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}
