package socketparts

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"

	"github.com/gorilla/websocket"
)

type Client struct {
	Conn   *websocket.Conn
	Pool   *Pool
	ID     string  `json:"id"`
	PoolID string  `json:"poolid"`
	Color  string  `json:"color"`
	Points int     `json:"points"`
	Left   float32 `json:"left"`
	Top    float32 `json:"top"`
	Right  float32 `json:"right"`
	Bottom float32 `json:"bottom"`
}
type Pool struct {
	Register   chan *Client
	Unregister chan *Client
	Clients    map[*Client]bool
	Broadcast  chan Message
	PoolID     string
	ClientID   string
}

type Message struct {
	Type       int    `json:"type"`
	Body       string `json:"body"`
	Clientdata string `json:"client"`
	ClientID   string `json:"clientid"`
	Client
}

var ClientShared []Client
var PoolIDs []string

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

func NewPool() *Pool {
	return &Pool{
		Register:   make(chan *Client),
		Unregister: make(chan *Client),
		Clients:    make(map[*Client]bool),
		Broadcast:  make(chan Message),
	}
}

func Upgrade(w http.ResponseWriter, r *http.Request) (*websocket.Conn, error) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	return conn, nil
}

func Writer(conn *websocket.Conn) {
	for {
		fmt.Println("Sending")
		messageType, r, err := conn.NextReader()
		if err != nil {
			fmt.Println(err)
			return
		}
		w, err := conn.NextWriter(messageType)
		if err != nil {
			fmt.Println(err)
			return
		}
		if _, err := io.Copy(w, r); err != nil {
			fmt.Println(err)
			return
		}
		if err := w.Close(); err != nil {
			fmt.Println(err)
			return
		}
	}
}

func (c *Client) Read() {
	//close client/conn
	defer func() {
		c.Pool.Unregister <- c
		c.Conn.Close()
	}()

	for {
		//take in a message
		messageType, p, err := c.Conn.ReadMessage()
		if err != nil {
			log.Println(err)
			return
		}

		message := Message{Type: messageType, Body: string(p)}

		c.Pool.Broadcast <- message
		fmt.Printf("Message Received: %+v\n", message)
	}
}

var Ids []string
var clientids []string
var Check bool
var createdid string

func GetClientID() string {
	ii := rand.Intn(100)
	createdid = fmt.Sprint(ii)
	return createdid
}
func contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}

//where the message gets written
func (pool *Pool) Start() {

	for {
		select {
		case client := <-pool.Register: //joining
			pool.Clients[client] = true
			fmt.Println("Size of Connection Pool: ", len(pool.Clients))
			pool.PoolID = "12"
			if pool.ClientID == "" {
				for client, _ := range pool.Clients {
					c := GetClientID()
					pool.ClientID = c
					Ids = append(Ids, c)
					var counter int
					for cc := range Ids {
						counter++
						fmt.Println(cc)
					}
					client.Conn.WriteJSON("Clientid:" + pool.ClientID + " Poolid:" + pool.PoolID + " Pool size:" + fmt.Sprint(counter))
				}
			}

			break
		case client := <-pool.Unregister: //leaving
			delete(pool.Clients, client)
			fmt.Println("Size of Connection Pool: ", len(pool.Clients))
			for client, _ := range pool.Clients {
				client.Conn.WriteJSON("New User Joined...")
			}
			break
		case message := <-pool.Broadcast: //tunnel of messages
			//where you can edit messages in the cycle
			//cycle through clients

			for client, _ := range pool.Clients {

				err := json.Unmarshal([]byte(message.Body), &client)
				if err != nil {
					panic(err)
				}
				Check := contains(clientids, client.ID)
				if !Check {
					clientids = append(clientids, client.ID)
					fmt.Println("!!!!333!!", clientids)
				}
				message.Client.Left = client.Left
				message.Client.Top = client.Top
				message.Client.Right = client.Right
				message.Client.Bottom = client.Bottom
				message.Client.ID = pool.ClientID
				message.PoolID = pool.PoolID

				ClientShared = append(ClientShared, message.Client)
				PoolIDs = append(PoolIDs, message.PoolID)

				if pool.PoolID == "12" {
					//encode and send
					if err := client.Conn.WriteJSON(message); err != nil { //send it
						fmt.Println(err)
						return
					}
				}

			} //end of range

		}
	}
}
