package alluser

import (
	"fmt"

	db "gitlab.com/zendrulat123/zegsite/db"
)

type User struct {
	ID    string `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Name  string `param:"name" query:"name" form:"name" json:"name" xml:"name" validate:"required" min:"4" max:"15" scrub:"name" mod:"trim"`
	Email string `param:"email" query:"email" form:"email" json:"email" xml:"email" validate:"required,email" mod:"trim"`
	Info  string `param:"info" query:"info" form:"info" json:"info" xml:"info"`
}

//CreateUser creates a Post
func GetAllDataUser() []User {
	//opening database
	data := db.Conn()

	var (
		id    string
		name  string
		email string
		info  string
		user  []User
	)
	i := 0
	//get from database
	rows, err := data.Query("select id, name, email, info from user")
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		err := rows.Scan(&id, &name, &email, &info)
		if err != nil {
			fmt.Println(err)
		} else {
			i++
			fmt.Println("scan ", i)
		}
		u := User{ID: id, Name: name, Email: email, Info: info}
		user = append(user, u)

	}
	defer rows.Close()
	defer data.Close()
	return user
}
