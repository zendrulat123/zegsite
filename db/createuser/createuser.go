package createuser

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	autho "gitlab.com/zendrulat123/zegsite/autho"
	conn "gitlab.com/zendrulat123/zegsite/db"
)

type User struct {
	ID    string `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Name  string `param:"name" query:"name" form:"name" json:"name" xml:"name" validate:"required" min:"4" max:"15" scrub:"name" mod:"trim"`
	Email string `param:"email" query:"email" form:"email" json:"email" xml:"email" validate:"required,email" mod:"trim"`
	Info  string `param:"info" query:"info" form:"info" json:"info" xml:"info"`
}

func UserCreation(c echo.Context) error {
	u := new(User)

	if err := c.Bind(u); err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
	}

	if u.Name == "" || u.Email == "" {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "invalid name or email"}
	}

	autho.ValidateStruct(u)

	data := conn.Conn()
	//check if username is taken
	var exists bool
	stmts := data.QueryRowContext(context.Background(), "SELECT EXISTS(SELECT 1 FROM user WHERE name=?)", u.Name)
	if err := stmts.Scan(&exists); err != nil {
		log.Fatal(err)
	}

	// if exists {
	// 	return c.Render(http.StatusOK, "form.html", map[string]interface{}{})
	// }

	stmt, err := data.Prepare("INSERT INTO user(name, email, info) VALUES( ?, ?, ?)")
	if err != nil {
		log.Fatal(err)
	}

	userstemp := User{Name: u.Name, Email: u.Email, Info: u.Info}
	fmt.Println(userstemp)
	res, err := stmt.Exec(userstemp.Name, userstemp.Email, userstemp.Info)
	if err != nil {
		log.Fatal(err)
	}
	lastId, err := res.LastInsertId()
	if err != nil {
		log.Fatal(err)
	}
	rowCnt, err := res.RowsAffected()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("ID = %d, affected = %d\n", lastId, rowCnt)
	defer data.Close()
	c.Redirect(http.StatusFound, "/")
	return c.Render(http.StatusOK, "home.html", map[string]interface{}{
		"ua": userstemp,
	})

}
